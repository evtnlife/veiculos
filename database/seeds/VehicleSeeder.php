<?php

use Illuminate\Database\Seeder;
use App\Vehicle;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicle = new Vehicle();
        $vehicle->model_id = 1;
        $vehicle->description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        $vehicle->photo = '/storage/img/veiculo-padrao.png';
        $vehicle->model_year = 2019;
        $vehicle->type = "Carro";
        $vehicle->license_plate = "ABC0C0";
        $vehicle->save();
        $vehicle = new Vehicle();
        $vehicle->model_id = 2;
        $vehicle->description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        $vehicle->photo = '/storage/img/veiculo-padrao.png';
        $vehicle->model_year = 2019;
        $vehicle->type = "Moto";
        $vehicle->license_plate = "ABC0C1";
        $vehicle->save();
        $vehicle = new Vehicle();
        $vehicle->model_id = 3;
        $vehicle->description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        $vehicle->photo = '/storage/img/veiculo-padrao.png';
        $vehicle->model_year = 2019;
        $vehicle->type = "Carro";
        $vehicle->license_plate = "ABC0C2";
        $vehicle->save();
    }
}
