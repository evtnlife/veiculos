<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brand = new Brand();
        $brand->name = "Audi";
        $brand->save();
        $brand = new Brand();
        $brand->name = "BMW";
        $brand->save();
    }
}
