<?php

use Illuminate\Database\Seeder;
use App\Model;

class ModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new Model();
        $model->name = "A3 Sport Back 1.8 T";
        $model->brand_id = 1;//audi
        $model->save();

        $model = new Model();
        $model->name = "G 310 GS";
        $model->brand_id = 2;//bmw
        $model->save();

        $model = new Model();
        $model->name = "320I";
        $model->brand_id = 2;
        $model->save();
    }
}
