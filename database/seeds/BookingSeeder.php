<?php

use Carbon\Traits\Date;
use Illuminate\Database\Seeder;
use App\BookingVehicle;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $booking = new BookingVehicle();
        $booking->rent_start = date_create("2021-02-11");
        $booking->rent_end = date_create("2021-02-14");
        $booking->vehicle_id = 1;
        $booking->user_id = 2;
        $booking->save();

        $booking = new BookingVehicle();
        $booking->rent_start = date_create("2021-02-16");
        $booking->rent_end = date_create('2021-02-17');
        $booking->vehicle_id = 1;
        $booking->user_id = 2;
        $booking->save();
    }
}
