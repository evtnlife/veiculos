<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->password = bcrypt("admin");
        $user->cpf = "333.333.333-33";
        $user->user_id = 1;
        $user->admin = true;
        $user->save();
        $user = new User();
        $user->name = "teste 1";
        $user->email = "teste1@admin.com";
        $user->password = bcrypt("admin");
        $user->cpf = "333.333.333-33";
        $user->admin = false;
        $user->user_id = 1;
        $user->save();
        $user = new User();
        $user->name = "teste 2";
        $user->email = "teste2@admin.com";
        $user->password = bcrypt("admin");
        $user->cpf = "333.333.333-33";
        $user->admin = false;
        $user->user_id = 1;
        $user->save();
    }
}
