## Instalação do Sistema

* Instale o PHP/Mysql em seu ambiente local(Eu costumo utilizar o Laragon que ja vem com tudo que é necessário).
* Crie um banco de dados(Nome de sua escolhe).
* Configure o arquivo .env no diretório raiz do projeto.
* Execute os comandos:
  * composer install
  * npm install
  * php artisan migrate:refresh --seed
  * npm run dev ou npm run watch
* Acesse a pagina para validar
* Usuarios de teste
  * user: admin@admin.com | pass: admin
  * user: teste1@admin.com | pass: admin
  * user: teste2@admin.com | pass: admin
