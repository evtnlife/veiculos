<?php

namespace App\Http\Controllers;

use App\BookingVehicle;
use App\Events\BookingRegisterLog;
use Illuminate\Http\Request;

class BookingVehicleController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request = $request->all()['booking'];
        if ($request['rent_start'] > $request['rent_end'])
            return response()->json(['status' => 'Data inicial inferior a data final'], 202);
        if ($request['vehicle_id'] <= 0)
            return response()->json(['status' => 'Veiculo invalido'], 202);
        if ($request['user_id'] <= 0)
            return response()->json(['status' => 'Usuário não encontrado'], 202);
        $booking = BookingVehicle::where(function ($query) use ($request) {
            $query->where('rent_start', '<=', $request['rent_start'])->where('rent_end', '>=', $request['rent_start']);
        })->orWhere(function ($query) use ($request) {
            $query->where('rent_start', '<=', $request['rent_end'])->where('rent_end', '>=', $request['rent_end']);
        })->orWhere(function ($query) use ($request) {
            $query->where('rent_start', '>=', $request['rent_start'])->where('rent_end', '<=', $request['rent_end']);
        })->get();
        if (count($booking) == 0) {
            $booking = new BookingVehicle();
            $booking->rent_start = $request['rent_start'];
            $booking->rent_end = $request['rent_end'];
            $booking->user_id = $request['user_id'];
            $booking->vehicle_id = $request['vehicle_id'];
            if ($booking->save()) {
                $bookings = BookingVehicle::with('User')->where('vehicle_id', $request['vehicle_id'])->get();
                event(new BookingRegisterLog($booking->user_id, $booking->vehicle_id));
                return response()->json(
                    [
                        'status' => 'Agendamento salvo com sucesso!',
                        'bookings' => $bookings
                    ]);
            }
        }
        return response()->json(['status' => 'Falha ao criar agendamento, a data escolhida ja está em uso!'], 202);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\BookingVehicle $bookingVehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $request = $request->all()['booking'];
        if ($request['rent_start'] > $request['rent_end'])
            return response()->json(['status' => 'Data inicial inferior a data final'], 202);
        if ($request['vehicle_id'] <= 0)
            return response()->json(['status' => 'Veiculo invalido'], 202);
        if ($request['user_id'] <= 0)
            return response()->json(['status' => 'Usuário não encontrado'], 202);
        $booking = BookingVehicle::where(function ($query) use ($request) {
            $query->where('rent_start', '<=', $request['rent_start'])->where('rent_end', '>=', $request['rent_start']);
        })->orWhere(function ($query) use ($request) {
            $query->where('rent_start', '<=', $request['rent_end'])->where('rent_end', '>=', $request['rent_end']);
        })->orWhere(function ($query) use ($request) {
            $query->where('rent_start', '>=', $request['rent_start'])->where('rent_end', '<=', $request['rent_end']);
        })->get();
        if (count($booking) == 0) {
            $booking = BookingVehicle::find($request['id']);
            $booking->rent_start = $request['rent_start'];
            $booking->rent_end = $request['rent_end'];
            $booking->user_id = $request['user_id'];
            $booking->vehicle_id = $request['vehicle_id'];
            if ($booking->save()) {
                $bookings = BookingVehicle::with('User')->where('vehicle_id', $request['vehicle_id'])->get();
                event(new BookingRegisterLog($booking->user_id, $booking->vehicle_id));
                return response()->json(
                    [
                        'status' => 'Agendamento salvo com sucesso!',
                        'bookings' => $bookings
                    ]);
            }
        }
        return response()->json(['status' => 'Falha ao criar agendamento, a data escolhida ja está em uso!'], 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\BookingVehicle $bookingVehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request = $request->all()['booking'];
        if (BookingVehicle::destroy($id)) {
            $bookings = BookingVehicle::with('User')->where('vehicle_id', $request['vehicle_id'])->get();
            return response()->json(['status' => 'Agendamento delatado com sucesso!', 'bookings' => $bookings]);
        }
        return response()->json(['status' => 'Falha ao deletar agendamento'], 202);
    }
}
