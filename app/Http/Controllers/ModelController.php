<?php

namespace App\Http\Controllers;

use App\Model;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModelsByBrandID($id)
    {
        $models = Model::where('brand_id', $id)->get();
        if(count($models) > 0)
            return response()->json(['models' => $models], 200);
        else
            return response()->json([], 202);
    }
}
