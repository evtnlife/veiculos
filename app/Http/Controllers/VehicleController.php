<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Middleware\IsAdmin;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->middleware('auth');
        $vehicles = Vehicle::with('Model')->get();
        return view('vehicle.list')->with(['vehicles' => $vehicles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->middleware(IsAdmin::class);
        $brands = Brand::all();
        return view('vehicle.create')->with(['brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->middleware(IsAdmin::class);
        $validatedData = $request->validate([
            'brand' => 'required',
            'model' => 'required',
            'description' => 'required',
            'license_plate' => 'required',
            'model_year' => 'required',
            'photo' => 'required',
            'type' => 'required'
        ]);

        $vehicle = new Vehicle();
        $vehicle->model_id = $validatedData['model'];
        $vehicle->description = $validatedData['description'];
        $vehicle->photo = '/storage/' . Storage::disk('public')->put('/img/', $request->file('photo'));
        $vehicle->model_year = $validatedData['model_year'];
        $vehicle->license_plate = $validatedData['license_plate'];
        $vehicle->type = $validatedData['type'];
        if ($vehicle->save())
            return redirect('/vehicle/list')->with(['status' => 'Veículo criado com sucesso!']);
        return redirect('/vehicle/list')->with(['status' => 'Falha ao criar veículo!']);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vehicle $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->middleware(IsAdmin::class);
        $brands = Brand::all();
        $vehicle = Vehicle::with('Model')->where('id', $id)->first();
        return view('vehicle.edit')->with([
            'vehicle' => $vehicle,
            'brands' => $brands
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Vehicle $vehicle
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->middleware(IsAdmin::class);
        $validatedData = $request->validate([
            'brand' => 'required',
            'model' => 'required',
            'description' => 'required',
            'license_plate' => 'required',
            'model_year' => 'required',
            'photo' => 'required',
            'type' => 'required'
        ]);
        $vehicle = Vehicle::find($id);
        $oldPhoto = $vehicle->path;
        $vehicle->model_id = $validatedData['model'];
        $vehicle->description = $validatedData['description'];
        $vehicle->photo = '/storage/' . Storage::disk('public')->put('/img/', $request->file('photo'));
        $vehicle->model_year = $validatedData['model_year'];
        $vehicle->license_plate = $validatedData['license_plate'];
        $vehicle->type = $validatedData['type'];
        if ($vehicle->save()) {
            Storage::delete($oldPhoto);
            return redirect('/vehicle/list')->with(['status' => 'Veículo criado com sucesso!']);
        }
        return redirect('/vehicle/list')->with(['status' => 'Falha ao criar veículo!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Vehicle $vehicle
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if(Vehicle::destroy($id))
            return redirect('/vehicle/list')->with(['status' => 'Veículo removido com sucesso!']);
        return redirect('/vehicle/list')->with(['status' => 'Falha ao remover veículo!']);
    }
    /**
     * Show the vehicle
     *
     * @param \App\Vehicle $vehicle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report($id)
    {
        $vehicle = Vehicle::with('Bookings')->with('Model')->where('id', $id)->first();
        return view('vehicle.report')->with(['vehicle' => $vehicle]);
    }
}
