<?php

namespace App;

use Illuminate\Database\Eloquent\Model as ModelLaravel;

class Model extends ModelLaravel
{
    protected $table = "models";

    protected $fillable = [
        'id',
        'brand_id',
        'name',
        'created_at',
        'updated_at',
    ];
    /**
     * Retorna a marca do modelo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @var array
     */
    public function Brand(){
        return $this->belongsTo(Brand::class,'brand_id', 'id');
    }
}
