<?php

namespace App\Listeners;

use App\Events\BookingRegisterLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SendBookingRegisterLog
{
    protected $user_id = 0;
    protected $vehicle_id = 0;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  BookingRegisterLog  $event
     * @return void
     */
    public function handle(BookingRegisterLog $event)
    {
        Log::info($event->getMessage());
    }
}
