<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable = [
        'id',
        'name',
        'created_at',
        'updated_at'
    ];
    /**
     * Retorna o Modelo do veiculo
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @var array
     */
    public function Models(){
        return $this->hasMany(Model::class, 'brand_id', 'id');
    }
}
