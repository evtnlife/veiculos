<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BookingRegisterLog
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user_id = 0;
    public $vehicle_id = 0;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $vehicle)
    {
        $this->user_id = $user;
        $this->vehicle_id = $vehicle;
    }

    public function getMessage(){
        return "Veiculo agendado - [UserID:".$this->user_id."|VeiculoID:".$this->vehicle_id."]";
    }
}
