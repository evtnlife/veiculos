<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingVehicle extends Model
{
    protected $table = 'booking_vehicles';

    protected $fillable = [
        'id',
        'rent_start',
        'rent_end',
        'user_id',
        'vehicle_id',
        'created_at',
        'updated_at'
    ];
    /**
     * Retorna o veiculo do agendamento
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @var array
     */
    public function Vehicle()
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_id', 'id');
    }
    /**
     * Retorna o usuario que realizou o agendamento.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @var array
     */
    public function User(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
