<?php

namespace App;

use Illuminate\Database\Eloquent\Model as ModelLaravel;

class Vehicle extends ModelLaravel
{
    protected $table = "vehicles";

    protected $fillable = [
        'id',
        'model_id',
        'description',
        'photo',
        'model_year',
        'type',
        'created_at',
        'updated_at'
    ];

    /**
     * Retorna o Modelo do veiculo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @var array
     */
    public function Model(){
        return $this->belongsTo(Model::class, 'model_id', 'id')->with('Brand');
    }
    public function Bookings(){
        return $this->hasMany(BookingVehicle::class, 'vehicle_id', 'id')->with('User')->orderBy('created_at');
    }
}
