@extends('layouts.app')

@section('content')
    <vehicle-report-component :p_vehicle="{{$vehicle}}" :p_user="{{Auth::user()}}"></vehicle-report-component>
@endsection
