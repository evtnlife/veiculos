@extends('layouts.app')

@section('content')
    <div class="container edit-content">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Editar Veículo</div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/vehicle/edit/{{$vehicle->id}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" id="vehicle_id" name="vehicle_id" value="{{$vehicle->id}}" />
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="brand">Marcas</label>
                                <select name="brand" id="brand" class="form-control" required>
                                    <option value="-1">Selecione uma marca</option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}" {{ $vehicle->Model->Brand->id == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="model">Modelos</label>
                                <select name="model" id="model" class="form-control" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="photo"></label>
                                <input type="file" class="form-control" id="photo" name="photo" required>
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Detalhes do Veículo" value="{{$vehicle->description}}" required>
                            </div>
                            <div class="form-group">
                                <label for="license_plate">Placa</label>
                                <input type="text" class="form-control" id="license_plate" name="license_plate"  placeholder="AAA0A0" value="{{$vehicle->license_plate}}" required>
                            </div>
                            <div class="form-group">
                                <label for="model_year">Ano Modelo</label>
                                <input type="number" class="form-control" id="model_year" name="model_year" max="2021" step="1" min="1900" placeholder="2012" value="{{$vehicle->model_year}}" required>
                            </div>
                            <div class="form-group">
                                <label for="type">Tipo</label>
                                <select name="type" id="type" class="form-control" required>
                                    <option value="Moto" {{$vehicle->type == 'Moto' ? 'selected' : ''}}>Moto</option>
                                    <option value="Carro" {{$vehicle->type != 'Moto' ? 'selected' : ''}}>Carro</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Editar Veículo</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
