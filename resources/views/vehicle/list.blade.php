@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Lista de Veículos</div>
                    <div class="card-body">
                        @if(session('status'))
                            <div class="alert alert-info">
                                {{session('status')}}
                            </div>
                        @endif
                        <table class="table table-hover table-responsive-lg">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Model</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Photo</th>
                                <th scope="col">Ano Modelo</th>
                                <th scope="col">Placa</th>
                                <th scope="col">Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vehicles as $vehicle)
                                <tr>
                                    <th scope="row">{{$vehicle->id}}</th>
                                    <td>{{$vehicle->Model->Brand->name}}</td>
                                    <td>{{$vehicle->Model->name}}</td>
                                    <td>{{$vehicle->type}}</td>
                                    <td><img src="{{$vehicle->photo}}" class="vehicle-figure" /></td>
                                    <td>{{$vehicle->model_year}}</td>
                                    <td>{{$vehicle->license_plate}}</td>
                                    <td>
                                        <a href="/report/vehicle/{{$vehicle->id}}">
                                            <i class="fas fa-calendar-alt"></i>
                                        </a> |
                                        <a href="/vehicle/edit/{{$vehicle->id}}">
                                            <i class="fas fa-edit"></i>
                                        </a> |
                                        <a href="/vehicle/destroy/{{$vehicle->id}}" onclick="
                                            event.preventDefault();
                                            $('#destroy-form').attr('action', '/vehicle/destroy/{{$vehicle->id}}');
                                            document.getElementById('destroy-form').submit();">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <form id="destroy-form" action="" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
