@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Apresentação do projeto</div>
                    <div class="card-body">
                        <h4>Sistema de locação de veículos.</h4>
                        <p class="text-justify">
                            Desenvolver um sistema para gerenciar uma locadora de veículos. Deve ser possível listar/criar/editar/remover usuários, assim como veículos. Deve ser possível criar, editar e remover reservas de veículos. Um veículo só pode estar reservado para um usuário por vez. Um usuário pode reservar diversos carros ao mesmo tempo.<br/>
                            Deseja-se uma visão (relatório/tabela) que exiba para um determinado veículo qual a sua reserva/disponibilidade para cada dia de determinado mês.
                        </p>

                        <hr/>
                        <h4>Funções do sistema</h4>
                        <hr/>

                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <h5>Usuários(Admin)</h5>
                                <ul>
                                    <li>Listar Todos</li>
                                    <li>Criar</li>
                                    <li>Editar</li>
                                    <li>Remover</li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <h5>Veículos</h5>
                                <ul>
                                    <li>Listar Todos</li>
                                    <li>Visualizar</li>
                                    <li>Criar(Admin)</li>
                                    <li>Editar(Admin)</li>
                                    <li>Remover(Admin)</li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <h5>Reservas</h5>
                                <ul>
                                    <li>Remover</li>
                                    <li>Adicionar</li>
                                    <li>Editar</li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <h5>Relatórios</h5>
                                <ul>
                                    <li>Agendamentos por Veiculo</li>
                                    <li>Veiculos MES</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
