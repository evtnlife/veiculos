@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Editar Usuário</div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/user/edit/{{$user->id}}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">Nome</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="*******" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="teste@teste.com" value="{{$user->email}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="teste@teste.com" value="{{$user->cpf}}" required>
                            </div>
                            <div class="form-group">
                                <label for="admin">Admin</label>
                                <select name="admin" class="form-control">
                                    <option value="0" {{!$user->admin ? 'selected' : ''}}>Não</option>
                                    <option value="1" {{$user->admin ? 'selected' : ''}}>Sim</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Editar Usuário</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
