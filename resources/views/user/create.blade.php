@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Editar Usuário</div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/user/create" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">Nome</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Digite seu nome" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Digite sua senha" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="exemplo@exemplo.com" value="{{old('email')}}" required>
                            </div>
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="333.333.333-33" value="{{old('cpf')}}" required>
                            </div>
                            <div class="form-group">
                                <label for="admin">Admin</label>
                                <select name="admin" class="form-control">
                                    <option value="0" {{old('admin') == 0 ? 'selected' : ''}}>Não</option>
                                    <option value="1" {{old('admin') == 1 ? 'selected' : ''}}>Sim</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Criar Usuário</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
