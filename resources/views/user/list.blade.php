@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Lista de Usuários</div>
                    <div class="card-body">
                        @if(session('status'))
                            <div class="alert alert-info">
                                {{session('status')}}
                            </div>
                        @endif
                        <table class="table table-hover table-responsive-lg">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                                <th scope="col">CPF</th>
                                <th scope="col">Nivel</th>
                                <th scope="col">Responsavel</th>
                                <th scope="col">Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{$user->id}}</th>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->cpf}}</td>
                                    <td>{{$user->admin == 1 ? 'Administrador' : 'Usuário'}}</td>
                                    <td>{{$user->Responsavel->name}}</td>
                                    <td>
                                        <a href="/user/edit/{{$user->id}}">
                                            <i class="fas fa-edit"></i>
                                        </a> |
                                        <a href="/user/destroy/{{$user->id}}" onclick="
                                            event.preventDefault();
                                            $('#destroy-form').attr('action', '/user/destroy/{{$user->id}}');
                                            document.getElementById('destroy-form').submit();">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <form id="destroy-form" action="" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
