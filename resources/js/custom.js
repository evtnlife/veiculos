$(document).ready(function () {
    $('#brand').trigger('change');

    $('#brand').change(function (e) {
        e.preventDefault();
        let id = $(this).val();
        $.ajax('/vehicle/models/' + id, {
            method: 'GET',
            success: function (response) {
                $("#model").empty();
                if (response !== undefined) {
                    $.each(response.models, function (item, obj) {
                        $("#model").append(new Option(obj.name, obj.id));
                    });
                    if ($('#vehicle_id') !== undefined) {
                        let id = $('#vehicle_id').val();
                        $('#model option').removeAttr('selected').filter('[value=' + id + ']').attr('selected', true);
                    }
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
});

$('.edit-content').ready(function () {
    $('#brand').trigger('change');
});
