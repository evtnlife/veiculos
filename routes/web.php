<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Rotas Usuarios
Route::get('/user/list', 'UserController@index');
Route::get('/user/create', 'UserController@create');
Route::post('/user/create', 'UserController@store');
Route::get('/user/edit/{id}', 'UserController@edit');
Route::post('/user/edit/{id}', 'UserController@update');
Route::post('/user/destroy/{id}', 'UserController@destroy');

//Rotas Veiculos
Route::get('/vehicle/list', 'VehicleController@index');
Route::get('/vehicle/show/{id}', 'VehicleController@show');
Route::get('/vehicle/create', 'VehicleController@create');
Route::post('/vehicle/create', 'VehicleController@store');
Route::get('/vehicle/edit/{id}', 'VehicleController@edit');
Route::post('/vehicle/edit/{id}', 'VehicleController@update');
Route::post('/vehicle/destroy/{id}', 'VehicleController@destroy');
Route::get('/vehicle/models/{id}', 'ModelController@getModelsByBrandID');

//Reservas
Route::post('/booking/create', 'BookingVehicleController@store');
Route::post('/booking/destroy/{id}', 'BookingVehicleController@destroy');
Route::post('/booking/edit/{id}', 'BookingVehicleController@update');

//Relatórios
Route::get('/report/vehicle/{id}', 'VehicleController@report');
